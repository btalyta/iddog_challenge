//
//  XCTestCase+extension.swift
//  IDdog ChallengeTests
//
//  Created by Bárbara Souza on 13/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    
    func waitTime(duration: TimeInterval, callback: @escaping () -> Void ) {
        let exp = expectation(description: "waitTime")
        DispatchQueue.main.asyncAfter(deadline: .now() + duration) { // change 2 to desired number of seconds
            exp.fulfill()
            callback()
        }
        waitForExpectations(timeout: duration + 1, handler: nil)
    }
}
