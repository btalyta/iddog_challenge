//
//  IDdog_ChallengeTests.swift
//  IDdog ChallengeTests
//
//  Created by Bárbara Souza on 10/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import XCTest
@testable import IDdog_Challenge

class IDdog_ChallengeTests: XCTestCase {
    var viewController: DogsViewController!
    var testUser: User = User(user:  UserData.init(_id: "01", email: "btas@gmail.com", createdAt: "2018-05-10T23:42:59.676Z", token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJpZGRvZyIsInN1YiI6IjVhZjRkOTAzNDdkNWFkMDA0ZmUzZTkxZiIsImlhdCI6MTUyNTk5NTc3OSwiZXhwIjoxNTI3MjkxNzc5fQ.1y4MEJGggTuKmP9RiOeuqZXm1qJoF-ICzpuQICDUi48"))
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController = UIStoryboard(name: "Dogs", bundle: nil).instantiateViewController(withIdentifier: "DogsController") as! DogsViewController
        viewController.viewModel = DogsViewModel(withUser: testUser)
        viewController.preload()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testNumberOfSection() {
        XCTAssertEqual(viewController.dogCollection.numberOfSections, 1, "Numero de seções na tabela deve ser igual a 1")
    }
    
    func testSegmentControl(){
        XCTAssertEqual(viewController.dogCategory.numberOfSegments, viewController.viewModel.getNumbersOfCategories(), "Numero de seções no segmentControl deve ser igual a 4")
        XCTAssertEqual(viewController.dogCategory.titleForSegment(at: 0)?.lowercased(),"husky", "Dados incorretos na primeira seção")
        XCTAssertEqual(viewController.dogCategory.titleForSegment(at: 1)?.lowercased(),"hound", "Dados incorretos na segunda seção")
        XCTAssertEqual(viewController.dogCategory.titleForSegment(at: 2)?.lowercased(),"pug", "Dados incorretos na terceira seção")
        XCTAssertEqual(viewController.dogCategory.titleForSegment(at: 3)?.lowercased(),"labrador", "Dados incorretos na quarta")
    }
    
    func testNumberOfRowsInSection(){
        self.viewController.viewModel.loadDogs()
        waitTime(duration: 30, callback: {
            XCTAssertEqual(self.viewController.dogCollection.numberOfItems(inSection: 0), self.viewController.viewModel.getNumbersOfDogs(toCategory: 0), "Numero de seções no segmentControl deve ser igual a 192")
            self.viewController.dogCategory.selectedSegmentIndex = 1
            self.viewController.dogCollection.reloadData()
            XCTAssertEqual(self.viewController.dogCollection.numberOfItems(inSection: 0), self.viewController.viewModel.getNumbersOfDogs(toCategory: 1), "Numero de seções no segmentControl deve ser igual a 157")
            
            self.viewController.dogCategory.selectedSegmentIndex = 2
            self.viewController.dogCollection.reloadData()
            XCTAssertEqual(self.viewController.dogCollection.numberOfItems(inSection: 0),self.viewController.viewModel.getNumbersOfDogs(toCategory: 2) , "Numero de seções no segmentControl deve ser igual a 200")
            
            self.viewController.dogCategory.selectedSegmentIndex = 3
            self.viewController.dogCollection.reloadData()
            XCTAssertEqual(self.viewController.dogCollection.numberOfItems(inSection: 0), self.viewController.viewModel.getNumbersOfDogs(toCategory: 3), "Numero de seções no segmentControl deve ser igual a 171")
            
        })

    }
    
}
