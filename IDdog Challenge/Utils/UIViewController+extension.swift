//
//  File.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 13/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func preload() {
        _ = self.view
    }
    
}
