//
//  REST.swift
//  Cars
//
//  Created by Bárbara Souza on 26/02/18.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation

enum DogError{
    case url
    case taskError(error: Error)
    case noResponse
    case noData
    case responseStatusCode(code: Int)
    case invalidJSON
}
class REST{
    
    private static let basePath = "https://iddog-api.now.sh/"
    
    class func setURLSessionConfiguration(withToken token: String) -> URLSessionConfiguration {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["Authorization": token,"Content-Type":"application/json"]
        config.timeoutIntervalForRequest = 30
        config.httpMaximumConnectionsPerHost = 5
        return config
    }
    
    class func loadDogs(withToken token: String, category: String , onComplete: @escaping (Dog) -> Void, onError: @escaping (DogError) -> Void){
        let urlString : String = basePath + "feed?category=" + category
        let session = URLSession(configuration: REST.setURLSessionConfiguration(withToken: token))

        guard let url = URL(string: urlString) else {
            onError(.url)
            return
        }
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                onError(.taskError(error: error!))
            }else{
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                if (response.statusCode == 200){
                    guard let data = data else {return}
                    do{
                        let dog = try JSONDecoder().decode(Dog.self, from: data)
                        onComplete(dog)
                    }catch{
                        print("\(error.localizedDescription)")
                        onError(.invalidJSON)
                    }
                }else{
                    onError(.responseStatusCode(code: response.statusCode))
                }
            }
        }
        dataTask.resume()
    }
    
 
    
    class func signUp(withEmail email: String, onComplete: @escaping (User) -> Void, onError: @escaping (Bool) -> Void) {
        let urlString : String = basePath + "signup"
        let session = URLSession(configuration: REST.setURLSessionConfiguration(withToken: " "))
        guard let url = URL(string: urlString) else {
            onError(false)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let data = ["email" : email]
        
        guard let json = try? JSONSerialization.data(withJSONObject: data, options: .prettyPrinted) else {
            onError(false)
            return
        }
        request.httpBody = json
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil{
                guard let response = response as? HTTPURLResponse else {
                    return
                }
                if (response.statusCode == 200){
                    guard let data = data else {return}
                    do{
                        let json =  try JSONDecoder().decode(User.self, from: data)
                        onComplete(json)
                    }catch{
                        print("\(error.localizedDescription)")
                        onError(false)
                    }
                }else{
                    print("Status inválido!")
                    onError(false)
                }
            }else{
                onError(false)
            }
        }
        dataTask.resume()
    }
}
