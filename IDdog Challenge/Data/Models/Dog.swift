//
//  Dog.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation

struct Dog: Codable {
    let list : [String]
    let category : String
}
