//
//  User.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 10/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

struct User: Codable {
    let user: UserData
}

struct UserData: Codable {
    let _id: String
    let email: String
    let createdAt: String
    let token: String
}
