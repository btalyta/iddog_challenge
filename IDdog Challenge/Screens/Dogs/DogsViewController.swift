//
//  DogsViewController.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

enum DogCellState {
    case Presented, Expanded, Collapsed
}

import UIKit

class DogsViewController: UIViewController {

    @IBOutlet weak var reloadDogsBtn: UIButton!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var imagePresented: UIImageView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var dogCollection: UICollectionView!
    @IBOutlet weak var dogCategory: UISegmentedControl!
    var viewModel: DogsViewModelProtocol!
    var cellState: DogCellState = .Collapsed

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        self.setDogCategory()
        self.dogCollection.delegate = self
        self.dogCollection.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func setDogCategory(){
        self.dogCategory.removeAllSegments()
        let numbersCategories = self.viewModel.getNumbersOfCategories()
        for index in 0...numbersCategories-1{
            self.dogCategory.insertSegment(withTitle: self.viewModel.getCategory(withIndex: index).uppercased(), at: index, animated: true)
        }
        self.dogCategory.selectedSegmentIndex = 0
        self.dogCategory.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadDogs()
    }

    @IBAction func changeCategory(_ sender: Any) {
        self.dogCollection.setContentOffset(.zero, animated: true)
        self.dogCollection.reloadData()
        self.cellState = .Collapsed
    }
    
    @IBAction func reloadDogs(_ sender: Any) {
        self.reloadDogsBtn.isHidden = true
        self.activityIndicator.startAnimating()
        self.errorMessage.isHidden = true
        self.viewModel.loadDogs()

    
    }
    
    @IBAction func closePreview(_ sender: Any) {
        self.animationImage(fromCell: self.dogCollection.cellForItem(at: self.viewModel.getIndexPath()) as! DogCollectionViewCell , fromCollectionView: self.dogCollection)
    }
    
    func animationImage(fromCell cell: DogCollectionViewCell, fromCollectionView collectionView: UICollectionView){
        switch self.cellState {
            case .Collapsed:
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.blurView.isHidden = false
                    self.imagePresented.image = cell.dogImage.image
                    self.blurView.alpha = 1.0
                }, completion: nil)
                self.cellState = .Presented
                break
            
            case .Expanded:
                collectionView.isScrollEnabled = true
                collectionView.reloadItems(at: collectionView.indexPathsForSelectedItems!)
                self.cellState = .Collapsed
                break
            
            case .Presented:
                UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
                    cell.frame = collectionView.bounds
                    collectionView.isScrollEnabled = false
                    self.blurView.isHidden = true
                    self.blurView.alpha = 0
                    self.cellState = .Expanded
                }, completion: nil)
                break
            
        }
    }
    
}
extension DogsViewController: DogsViewModelDelegate{
    func didLoad(sucess: Bool) {
        if (sucess){
            DispatchQueue.main.async {
                self.dogCollection.reloadData()
                self.dogCategory.isEnabled = true
                self.activityIndicator.stopAnimating()
            }
        }else{
            DispatchQueue.main.async {
                self.reloadDogsBtn.isHidden = false
                self.activityIndicator.stopAnimating()
                self.errorMessage.isHidden = false
                self.errorMessage.text = self.viewModel.getErrorMessage()

            }
        }
    }
}

extension DogsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getNumbersOfDogs(toCategory: self.dogCategory.selectedSegmentIndex)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dogCell", for: indexPath) as! DogCollectionViewCell
        cell.prepareCell(withimageURL: self.viewModel.getDogs(toCategory: self.dogCategory.selectedSegmentIndex, withIndex: indexPath.item))
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! DogCollectionViewCell
        cell.superview?.bringSubview(toFront: cell)
        self.viewModel.setIndexPath(toIndex: indexPath)
        self.animationImage(fromCell: cell, fromCollectionView: collectionView)
    }
    
}
