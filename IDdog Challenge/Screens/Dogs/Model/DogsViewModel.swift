//
//  DogsViewModel.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation

protocol DogsViewModelProtocol {
    
    var delegate: DogsViewModelDelegate? {get set}
    
    func loadDogs()
    func getCategory(withIndex index: Int) -> String
    func getNumbersOfCategories() -> Int
    func getDogs(toCategory category: Int, withIndex index: Int) -> String
    func getNumbersOfDogs(toCategory category: Int) -> Int
    func setIndexPath(toIndex index: IndexPath)
    func getIndexPath() -> IndexPath
    func getErrorMessage() -> String
}
