//
//  DogsViewModelProtocol.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import UIKit

class DogsViewModel: DogsViewModelProtocol {
    
    weak var delegate: DogsViewModelDelegate?
    private var dogs: [Dog] = []
    private var categories: [String] = ["husky","hound","pug","labrador"]
    private var categoriesIndex: Int = 0
    private var currentUser: User!
    private var currentIndexPath: IndexPath!
    private var errorMessage: String!
    
    init(withUser user: User) {
        self.currentUser = user
    }
    
    func loadDogs() {
        REST.loadDogs(withToken: self.currentUser.user.token, category: self.categories[self.categoriesIndex] , onComplete: { (dogsLoaded) in
            self.dogs.append(dogsLoaded)
            if (self.categoriesIndex < self.categories.count-1){
                self.categoriesIndex+=1
                self.loadDogs()
            }else{
                self.delegate?.didLoad(sucess: true)
            }
        }) { (error) in
            switch (error){
            case .url:
                self.errorMessage = "Invalid url."
            case .taskError(let error):
                self.errorMessage = "Task error. Connection problems." 
            case .noResponse:
                self.errorMessage = "No response."
            case .noData:
                self.errorMessage = "No data."
            case .responseStatusCode(let code):
                self.errorMessage = "Response error status code:" + String(code.hashValue)
            case .invalidJSON:
                self.errorMessage = "Invalid JSON."
            }
            
            self.delegate?.didLoad(sucess: false)
        }
    }
    
    func getCategory(withIndex index: Int) -> String{
        return self.categories[index]
    }
    
    func getNumbersOfCategories() -> Int{
        return self.categories.count
    }
    
    func getDogs(toCategory category: Int, withIndex index: Int) -> String {
        return self.dogs[category].list[index]
    }
    
    func getNumbersOfDogs(toCategory category: Int) -> Int{
        return self.dogs.count != 0 ? self.dogs[category].list.count : 0
    }
    
    func setIndexPath(toIndex index: IndexPath){
        self.currentIndexPath = index
    }
    
    func getIndexPath() -> IndexPath{
        return self.currentIndexPath
    }
    
    func getErrorMessage() -> String{
        return self.errorMessage
    }

}
