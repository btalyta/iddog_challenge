//
//  DogCollectionViewCell.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import UIKit
import Kingfisher

class DogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dogImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func prepareCell(withimageURL imageURL: String){
        if let url = URL(string: imageURL){
            self.dogImage.kf.indicatorType = .activity
            self.dogImage.kf.setImage(with: url)
        }else{
            self.dogImage.image = nil
        }
        self.dogImage.clipsToBounds = true
    }
}
