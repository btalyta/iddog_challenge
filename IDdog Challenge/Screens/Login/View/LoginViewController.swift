//
//  ViewController.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 10/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var tfEmail: UITextField!
    
    var viewModel: LoginViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = LoginViewModel()
        self.viewModel.delegate = self
        self.blurView.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowTextDescription), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideTextDescription), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tfEmail.resignFirstResponder()
    }

    @IBAction func loginPress(_ sender: Any) {
        self.tfEmail.resignFirstResponder()
        if (self.tfEmail.text == "") {
            let alertController = UIAlertController(title: "The IDDog", message: "Favor inserir email.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.viewModel.login(withEmail: self.tfEmail.text!)
            self.blurView.isHidden = false
            self.activityIndicator.startAnimating()

        }
    }
    
    
    @IBAction func donePress(_ sender: Any) {
        self.loginPress(self)
        self.tfEmail.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueLoginViewToDogsView"){
            let dogsViewNavigation : UINavigationController = segue.destination as! UINavigationController
            let dogsView : DogsViewController  = dogsViewNavigation.viewControllers.first as! DogsViewController
            dogsView.viewModel = DogsViewModel(withUser: self.viewModel.getUser()!)
        }
    }
    
    @objc func keyboardWillShowTextDescription(notification: NSNotification) {
        
        if(self.tfEmail.isFirstResponder && (UIDevice.current.orientation != .portrait)){
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0{
                    if(self.tfEmail.isFirstResponder == true){
                        self.view.frame.origin.y -= keyboardSize.height/2
                    }else{
                        self.view.frame.origin.y = 0
                    }
                    
                }
            }
        }
    }
    
    @objc func keyboardWillHideTextDescription(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    }
    
}

extension LoginViewController: LoginViewModelDelegate{
    
    func didLogin(isLogged: Bool){
        if (isLogged){
            self.performSegue(withIdentifier: "segueLoginViewToDogsView", sender: self)
        }else{
            let alertController = UIAlertController(title: "The IDDog", message: "Não foi possivel realizar login, email incorreto. Tente novamente", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion:{
                DispatchQueue.main.async {
                    self.blurView.isHidden = true
                    self.activityIndicator.stopAnimating()
                }
            })
        }
    }

}
