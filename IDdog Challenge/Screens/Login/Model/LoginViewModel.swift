//
//  LoginViewModel.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import UIKit

class LoginViewModel: LoginViewModelProtocol {
    
    weak var delegate: LoginViewModelDelegate?
    private var currentUser: User?
    
    
    func login(withEmail email: String) {
        REST.signUp(withEmail: email, onComplete: { (user) in
            self.currentUser = user
            self.delegate?.didLogin(isLogged: true)
            
        }) { (error) in
            print(error)
            self.delegate?.didLogin(isLogged: false)
        }
    }

    func getUser() -> User?{
        return self.currentUser
    }

    
}
