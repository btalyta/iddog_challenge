//
//  LoginViewModelProtocol.swift
//  IDdog Challenge
//
//  Created by Bárbara Souza on 12/05/2018.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import Foundation

protocol LoginViewModelProtocol {
    
    var delegate: LoginViewModelDelegate? {get set}
    
    func login(withEmail email: String)
    
    func getUser() -> User?
    
}

